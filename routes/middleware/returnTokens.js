const router = require('express').Router();
const jwt = require('jsonwebtoken');
const secrets = require('../../secrets.js');

// function to return tokens, when proper request for them is issued
function sendTokens(req, res, next) {
    
    const accessPayload = {
        id: res.locals.userId,
        admin: res.locals.admin
    };

    const accessToken = jwt.sign(accessPayload, secrets.accessSecret, {
        expiresIn: 7200, // 2 hours
        issuer: 'test_assignment'
    });

    const refreshPayload = {
        id: res.locals.userId
    };
    const refreshToken = jwt.sign(refreshPayload, secrets.refreshSecret, {
        issuer: 'test_assignment'
    });

    const response = {
        'access-token': accessToken,
        'refresh-token': refreshToken
    };
    
    // optional text sent with tokens
    if (res.locals.comment)
        response.comment = res.locals.comment;

    res.status(200).send(response);
}

module.exports = sendTokens;
