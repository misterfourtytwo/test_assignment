// middleware check, whether userdata is non-empty and user is admin
module.exports = (req, res, next) => {

    if ( !res.locals.admin ) 
        return res.status(500).send({ 'error': 'error finding rights info' });
    if ( res.locals.admin != "true" ) 
        return res.status(403).send({ 'error': 'user have unsufficient rights.' });
    
    next();
};
