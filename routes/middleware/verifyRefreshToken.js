module.exports = function(db) {
    const jwt = require('jsonwebtoken');
    const secret = require('../../secrets.js').refreshSecret;

    // ObjectId cast is required for mongo request 
    const ObjectId = require('mongodb').ObjectId;

    // func to chk refresh-token validity
    return function verifyRefreshToken(req, res, next) {
        // chk whether token is provided at all
        const token = req.headers['refresh-token'];
        if (!token)
            return res.status(403).send({ 'error': 'no token provided.' });
        
        jwt.verify(token, secret, {issuer:'test_assignment'}, (err, decoded) => {
            // drop request if validation failed 
            if (err) {
                console.log(err);
                return res.status(401).send({ 'error': 'provided token invalid.' });
            }

            // find user token was provided for
            db.collection('users').findOne({'_id': new ObjectId(decoded.id) }, { _id:1, admin: 1 })
            .then( result => {
                // user coud've been deleted
                if (!result) 
                    return res.status(404).send({ 'error': 'user not found.' });
                
                // return userdata
                res.locals.comment = 'tokens reissued.';
                res.locals.userId = result._id;
                res.locals.admin = result.admin;
                
                // proceed request
                next();
            }).catch( err => {
                // db fail
                console.log(err);
                res.status(500).send({ 'error': 'error finding user.' });
            }) 
        });
    };
};

