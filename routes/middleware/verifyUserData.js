// middleware chk if userdata is provided
module.exports = function verifyUserData(req, res, next) {
    const user = {
        login: req.body.login,
        password: req.body.password,
        admin: req.body.admin
    };

    if (!user || !user.login || !user.password)
        return res.status(401).send({ 'error': 'user login and password must be specified.' });
    if (!user.admin)
        req.body.admin = 'false';

    next();
};
