module.exports = function(db) {
    const jwt = require('jsonwebtoken');
    const secret = require('../../secrets.js').accessSecret;
    
    // ObjectId cast is required for mongo requests
    const ObjectId = require('mongodb').ObjectId;
    
    // func to check access-token validity
    return function verifyAccessToken(req, res, next) {
        // chk whether token is provided at all
        const token = req.headers['access-token'];
        if (!token)
            return res.status(403).send({ 'error': 'no token provided.' });
        
        // check expiration, issuer and secret of a token
        jwt.verify(token, secret, {issuer:'test_assignment', ignoreExpiration: false }, (err, decoded) => {
            // drop request if validation failed
            if (err) {
                console.log(err);
                return res.status(401).send({ 'error': 'provided token invalid.' });
            }

            // find user token was provided for
            db.collection('users').findOne({'_id': new ObjectId(decoded.id) }, { _id:1, admin: 1 })
            .then( result => {
                // user coud've been deleted
                if (!result) 
                    return res.status(404).send({ 'error': 'user not found.' });
                
                // return userdata
                res.locals.comment = 'access token verified.';
                res.locals.userId = result._id;
                res.locals.admin = result.admin;

                // proceed request
                next();
            }).catch( err => {
                // db fail
                console.log(err);
                res.status(500).send({ 'error': 'error finding user.' });
            }) 
        });
    };
};

