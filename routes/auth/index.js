const RegisterRouter = require('./register');
const LoginRouter = require('./login');
const LogoutRouter = require('./logout');
const TokenRefreshRouter = require('./tokenRefresh');

// all subroutes specified here, thus can be turned on/off easily
module.exports = function (db) {
    // db argument is necessary so we can create routes after db connection 
    // was instantiated, so every route will use one common connection, thus
    // won't need to create it for every single request
    return [
        RegisterRouter(db),
        LoginRouter(db),
        LogoutRouter(db),
        TokenRefreshRouter(db)
    ];
};
