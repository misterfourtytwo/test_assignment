module.exports = function(db) {
    const router = require('express').Router();

    const verifyToken = require('../middleware/verifyRefreshToken')(db);
    const returnTokens = require('../middleware/returnTokens');

    router.post( '/token_refresh',
        verifyToken,
        // add comment msg
        (req, res, next) => {
            res.locals.comment = 'refresh token verified.';
            next();
        },
        // send new user credentials
        returnTokens            
    );
    return router;
};
