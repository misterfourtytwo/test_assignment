const router = require('express').Router();
const verifyUserData = require('../middleware/verifyUserData');
const returnTokens = require('../middleware/returnTokens');

module.exports = function(db) {
    router.post( '/register',
        verifyUserData, 
        // ask db to add new user
        (req, res, next) => {
            db.collection('users')
                .findOne({ login:req.body.login }).then( result => {

                    // username coud've been already taken
                    if (result != null)
                        return res.status(403).send({ 'error': 'user with that login already exists.' });

                    // create user object
                    const user = {
                        login: req.body.login,
                        password: req.body.password,
                        admin: (req.body.admin === 'true' ? 'true' : 'false')
                    };
                
                    // put into db
                    db.collection('users')
                    .insertOne(user).then( result => {
                        
                        // userdata necessary for response
                        res.locals.comment = 'user created.';
                        res.locals.userId = result.insertedId;
                        res.locals.admin = user.admin;

                        next();
                    }).catch( err => {
                        // db insertion fail
                        console.log(err);
                        res.status(500).send({ 'error': 'error creating user.' });
                    });

                }).catch( err => {
                    // db fail
                    console.log(err);
                    res.status(500).send({ 'error': 'error connecting to db.'});
                });
        },
        // send user credentials
        returnTokens
    );
    return router;
};
