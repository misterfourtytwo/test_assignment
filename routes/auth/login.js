module.exports = function(db) {
    const router = require('express').Router();
    const verifyUserData = require('../middleware/verifyUserData');
    const returnTokens = require('../middleware/returnTokens');

    // user login route
    router.post( ['/login', '/'],
        verifyUserData, 
        // ask db for provided user
        (req, res, next) => {
            const user = { login: req.body.login };
            db.collection('users').findOne(user, {admin: 1})
            .then( result => {

                if (!result) 
                    return res.status(404).send({ 'error': 'user not found.' });
                if (result.password != req.body.password)
                    return res.status(401).send({ 'error': 'wrong password.' });
                
                //projection not working, returns whole object, wtf
                //console.log(result);
                
                // userdata necessary for response
                res.locals.comment = 'user logged in.';
                res.locals.userId = result._id;
                res.locals.admin = result.admin;

                next();
            }).catch( err => {
                // db fail
                console.log(err);
                res.status(500).send({ 'error': 'error finding user.' });
            });
        },
        // send user credentials
        returnTokens
    );
    return router;
}
