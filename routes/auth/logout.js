module.exports = function(db) {
    const router = require('express').Router();

    // nullify tokens on frontend
    router.all('/logout', (req, res) => {
        res.status(200).send({ comment: "user logged out.", "access-token": null, "refresh-token": null });
    });
    return router;
}
