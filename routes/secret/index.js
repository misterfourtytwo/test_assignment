const secretRouter = require('./secret');
const adminRouter = require('./admin');

// all data subroutes specified here, thus can be managed easily
module.exports = function (db) {
    // db argument is necessary so every route won't create db connection for
    // itself for every response. though, it aren't necessary for every route,
    // i decided to maintain consistent interface. also, if any route will be
    // modified in the future, i won't need to go through all files changing
    // its creation signature
    return [
        secretRouter(db),
        adminRouter(db)
    ];
};
