module.exports = function( db ) {
    const router = require('express').Router();
    const verifyAccessToken = require('../middleware/verifyAccessToken')(db);
    const verifyAdmin = require('../middleware/verifyAdmin');

    // verify that user have admin rights and return data
    router.get( '/admin',
        verifyAccessToken,
        verifyAdmin,
        (req, res) => {
            res.status(200).send({ 'data': 'lesser secret is a lie.' });
        }
    );
    return router;
}
