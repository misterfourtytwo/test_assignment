module.exports = function(db) {
    const router = require('express').Router();
    const verifyAccessToken = require('../middleware/verifyAccessToken')(db);

    // verify that user logged in and return data
    router.get( '/',
        verifyAccessToken,
        (req, res) => {
            res.status(200).send({'data':'there is a bigger secret.'});
        }
    );
    return router;
}
