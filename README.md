### node.js jwt-auth example

uses express + mongo

#### routes:
* **/auth** - POST - redirect to /auth/login
 
* **/auth/register** - POST - for {login, password, [admin]}, if payload is correct
                          adds user to db and issue tokens for him
                     
* **/auth/login** - POST - for {login, password}, if authenticated, issues user tokens
 
* **/auth/logout** - ANY - returns null tokens
 
* **/auth/token_refresh** - POST - for valid refresh-token reissues user tokens
 
* **/secret** - GET - middleware checks access-token, responds if token verified
 
* **/secret/admin** - GET - besides to token verification, checks whether user is admin



