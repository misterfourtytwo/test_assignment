// import express and create its app
const express = require("express");
const app = express();
// add middleware for request parsing
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// define port to run app on
const port = process.argv[2] ? process.argv[2] : 8080;

// import db-access credentials, encryption secret keys
const secrets = require("./secrets");
const MongoClient = require("mongodb").MongoClient;
const dbUrl = `mongodb+srv://${secrets.dbCred}@clust-lnq4b.mongodb.net/`;

// instantiate the connection to database for the whole app
MongoClient.connect(dbUrl, { useNewUrlParser: true })
  .then(database => {
    db = database.db(secrets.dbName);
    // import and add auth routes to an app
    var authController = require("./routes/auth/index.js")(db);
    app.use("/auth", authController);
    // import and add data routes
    var secretController = require("./routes/secret")(db);
    app.use("/secret", secretController);

    // start listening on localhost:port, print status msg
    app.listen(port, () => {
      console.log("We are live on " + port);
    });
  })
  .catch(err => {
    return console.log(err);
  });
