var request = require("request");
//require("request-debug")(request);
const async = require("async");

const host =
  process.env.PORT !== undefined
    ? "http://localhost:" + process.env.PORT + "/"
    : "http://localhost:8080/";

console.log("running tests on " + host);

const secrets = require("./../secrets");
const MongoClient = require("mongodb").MongoClient;
const dbUrl = `mongodb+srv://${secrets.dbCred}@clust-lnq4b.mongodb.net/`;

MongoClient.connect(dbUrl, { useNewUrlParser: true })
  .then(database => {
    db = database.db(secrets.dbName);
    const testerObj = new require("./dbApi");
    testerObj.db = db;
    const responses = require("./responses")(testerObj);

    var test_queue = [];
    var testnum = 0;
    var errnum = 0;

    for (let response of responses) {
      for (let endpoint of response.routes) {
        var closure = (response, endpoint) => {
          return cb => {
            console.log(
              "####################################################################################################"
            );
            console.log("test №" + testnum);
            console.log("test endpoint: " + endpoint.url);
            console.log(
              "----------------------------------------------------------------------------------------------------"
            );

            let error = false;
            testerObj.payload = endpoint.payload;
            testerObj.headers = endpoint.headers;
            testerObj.series = [];

            if (endpoint.prerun) testerObj.series.push(endpoint.prerun);

            testerObj.series.push(cb => {
              var options = {
                url: endpoint.url,
                baseUrl: host,
                method: endpoint.method ? endpoint.method : "POST",
                form: endpoint.payload,
                headers: endpoint.headers
              };

              request(options, (err, res, body) => {
                console.log("request error = ", JSON.stringify(err));
                console.log(
                  "----------------------------------------------------------------------------------------------------"
                );
                if (res.statusCode != response.status_code) {
                  console.log("ERROR: response status codes do not match.");
                  console.log(`res.statusCode = ${res.statusCode}`);
                  console.log(`response.status_code = ${response.status_code}`);
                  error = true;
                } else console.log("response codes match.");
                console.log(
                  "----------------------------------------------------------------------------------------------------"
                );
                body = JSON.parse(body);

                console.log("response = ", JSON.stringify(response.body));
                let bodies_equal = true;

                for (var prop in response.body) {
                  if (!response.body.hasOwnProperty(prop)) continue;

                  console.log(" --- property of response: " + prop);
                  console.log("awaited property value: " + response.body[prop]);
                  console.log("fetched property value: " + body[prop]);
                  if (
                    !body.hasOwnProperty(prop) ||
                    (response.body[prop] !== undefined &&
                      response.body[prop] !== body[prop]) ||
                    (response.body[prop] === undefined && null === body[prop])
                  ) {
                    bodies_equal = false;
                    break;
                  }
                }

                if (!bodies_equal) {
                  console.log("ERROR: response bodies do not match.");
                  console.log(`fetched response: `);
                  console.log(body);
                  console.log(`awaited response: `);
                  console.log(response.body);
                  error = true;
                } else console.log("response bodies match.");

                console.log(
                  "----------------------------------------------------------------------------------------------------"
                );
                if (error) errnum++;
                cb();
              });
            });

            if (endpoint.postrun) testerObj.series.push(endpoint.postrun);

            async.series(testerObj.series, () => {
              if (!error)
                console.log(`test №${testnum++} finished successfully.`);
              else console.log(`test №${testnum++} finished with error.`);
              console.log(
                "####################################################################################################"
              );
              testerObj.payload = {};
              testerObj.headers = {};
              cb();
            });
          };
        };
        console.log(testnum++ + " test generated.");
        test_queue.push(closure(response, endpoint));
      }
    }

    testnum = 0;
    console.log("whole test queue generated.");
    async.series(test_queue, () => {
      console.log("whole test queue done.");
      console.log("total errors: " + errnum);
    });
  })
  .catch(err => {
    return console.log(err);
  });
