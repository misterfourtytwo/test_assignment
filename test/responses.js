module.exports = function(test) {
  return [
    {
      status_code: "200",
      body: {
        comment: "user logged out.",
        "access-token": undefined,
        "refresh-token": "THIS TEST SHALL NOT PASS."
      },
      routes: [
        {
          url: "/auth/logout"
        }
      ]
    },
    {
      status_code: "200",
      body: {
        comment: "THIS TEST MUST FAIL.",
        "access-token": null,
        "refresh-token": null
      },
      routes: [
        {
          url: "/auth/logout"
        }
      ]
    },
    {
      status_code: "200",
      body: {
        comment: "user logged out.",
        "access-token": null,
        "refresh-token": null
      },
      routes: [
        {
          url: "/auth/logout"
        }
      ]
    },
    {
      status_code: "200",
      body: {
        comment: "user created.",
        "access-token": undefined,
        "refresh-token": undefined
      },
      routes: [
        {
          url: "/auth/register",
          payload: {
            login: "AAAAA",
            password: "AAAAA",
            admin: "true"
          },
          postrun: function(cb) {
            test.remove_user(cb, { login: "AAAAA" });
          }
        }
      ]
    },
    {
      status_code: "200",
      body: {
        comment: "user logged in.",
        "access-token": undefined,
        "refresh-token": undefined
      },
      routes: [
        {
          url: "/auth/login",
          payload: {
            login: "test",
            password: "test"
          },
          prerun: function(cb) {
            test.create_user(cb);
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "200",
      body: {
        comment: "refresh token verified.",
        "access-token": undefined,
        "refresh-token": undefined
      },
      routes: [
        {
          url: "/auth/token_refresh",
          headers: {
            "refresh-token": null
          },
          prerun: function(cb) {
            test.create_user(err => {
              test.get_tokens((err, res) => {
                test.headers["refresh-token"] = res["refresh-token"];
                cb(err);
              });
            });
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "200",
      body: { data: "lesser secret is a lie." },
      routes: [
        {
          url: "/secret/admin",
          method: "GET",
          headers: {
            "access-token": null
          },
          prerun: function(cb) {
            test.create_user(
              err => {
                test.get_tokens((err, res) => {
                  test.headers["access-token"] = res["access-token"];
                  cb(err);
                });
              },
              { admin: "true" }
            );
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "200",
      body: { data: "there is a bigger secret." },
      routes: [
        {
          url: "/secret",
          method: "GET",
          headers: {
            "access-token": null
          },
          prerun: function(cb) {
            test.create_user(err => {
              test.get_tokens((err, res) => {
                test.headers["access-token"] = res["access-token"];
                cb(err);
              });
            });
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "401",
      body: { error: "user login and password must be specified." },
      routes: [
        // /auth/login, /auth
        {
          url: "/auth/login",
          payload: { login: "", password: "" }
        },
        {
          url: "/auth/login",
          payload: { login: "aaaaaa", password: "" }
        },
        {
          url: "/auth/login",
          payload: { login: "", password: "aaaaaa" }
        },
        {
          url: "/auth",
          payload: { login: "", password: "" }
        },
        {
          url: "/auth",
          payload: { login: "aaaaaa", password: "" }
        },
        {
          url: "/auth",
          payload: { login: "", password: "aaaaaa" }
        },

        // /auth/register
        {
          url: "/auth/register",
          payload: { login: "", password: "" }
        },
        {
          url: "/auth/register",
          payload: { login: "aaaaaa", password: "" }
        },
        {
          url: "/auth/register",
          payload: { login: "", password: "aaaaaa" }
        }
      ]
    },
    {
      status_code: "401",
      body: { error: "provided token invalid." },
      routes: [
        {
          url: "/auth/token_refresh",
          headers: {
            "refresh-token":
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjOGQzNzQyNjY0MjQ2M2IwOTBlZWI5ZCIsImFkbWluIjoiZmFsc2UiLCJpYXQiOjE1NTI3NTg1OTQsImV4cCI6MTU1Mjc2NTc5NCwiaXNzIjoidGVzdF9hc3NpZ25tZW50In0.4rXQHtovM1XQTcbJtQt6fOJgWikR4AqQKq8qp33Qxz"
          }
        },
        {
          url: "/secret/admin",
          method: "GET",
          headers: {
            "access-token":
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjOGQzNzQyNjY0MjQ2M2IwOTBlZWI5ZCIsImFkbWluIjoiZmFsc2UiLCJpYXQiOjE1NTI3NTg1OTQsImV4cCI6MTU1Mjc2NTc5NCwiaXNzIjoidGVzdF9hc3NpZ25tZW50In0.4rXQHtovM1XQTcbJtQt6fOJgWikR4AqQKq8qp33Qxz"
          }
        },
        {
          url: "/secret",
          method: "GET",
          headers: {
            "access-token":
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjOGQzNzQyNjY0MjQ2M2IwOTBlZWI5ZCIsImFkbWluIjoiZmFsc2UiLCJpYXQiOjE1NTI3NTg1OTQsImV4cCI6MTU1Mjc2NTc5NCwiaXNzIjoidGVzdF9hc3NpZ25tZW50In0.4rXQHtovM1XQTcbJtQt6fOJgWikR4AqQKq8qp33Qxz"
          }
        }
      ]
    },
    {
      status_code: "401",
      body: { error: "wrong password." },
      routes: [
        {
          url: "/auth/login",
          payload: {
            login: "test",
            password: "not-test"
          },
          prerun: function(cb) {
            test.create_user(cb, { login: "test", password: "test" });
          },
          postrun: function(cb) {
            test.remove_user(cb, { login: "test" });
          }
        }
      ]
    },
    {
      status_code: "403",
      body: { error: "user with that login already exists." },
      routes: [
        {
          url: "/auth/register",
          payload: {
            login: "test",
            password: "test"
          },
          prerun: function(cb) {
            test.create_user(cb);
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "403",
      body: { error: "no token provided." },
      routes: [
        {
          url: "/auth/token_refresh"
        },
        {
          url: "/secret/admin",
          method: "GET"
        },
        {
          url: "/secret",
          method: "GET"
        }
      ]
    },
    {
      status_code: "403",
      body: { error: "user have unsufficient rights." },
      routes: [
        {
          url: "/secret/admin",
          method: "GET",
          headers: { "access-token": null },
          prerun: function(cb) {
            test.create_user(
              err => {
                test.get_tokens((err, res) => {
                  test.headers["access-token"] = res["access-token"];
                  cb();
                });
              },
              { admin: "false" }
            );
          },
          postrun: function(cb) {
            test.remove_user(cb);
          }
        }
      ]
    },
    {
      status_code: "404",
      body: { error: "user not found." },
      routes: [
        // /auth, /auth/login -- no user with specified name.
        {
          url: "/auth/login",
          payload: {
            login: "USER_NOT_FOUND",
            password: "ANY"
          }
        },
        // user were deleted after token issue
        // /auth/token_refresh
        {
          url: "/auth/token_refresh",
          headers: {
            "refresh-token": null
          },

          prerun: function(cb) {
            test.create_user(err => {
              test.get_tokens((err, res) => {
                test.headers["refresh-token"] = res["refresh-token"];
                test.remove_user(cb);
              });
            });
          }
        },
        {
          url: "/secret/admin",
          method: "GET",
          headers: {
            "access-token": null
          },
          prerun: function(cb) {
            test.create_user(err => {
              test.get_tokens((err, res) => {
                test.headers["access-token"] = res["access-token"];
                test.remove_user(cb);
              });
            });
          }
        },
        {
          url: "/secret",
          method: "GET",
          headers: {
            "access-token": null
          },
          prerun: function(cb) {
            test.create_user(err => {
              test.get_tokens((err, res) => {
                test.headers["access-token"] = res["access-token"];
                test.remove_user(cb);
              });
            });
          }
        }
      ]
    }
    /*,{
        status_code:'500',
        body: {'error': 'error finding user.'},
        routes: [
            {
                url: '/auth/login'
                payload: {
                    login: 'test',
                    password: 'test'
                },
                prerun: function() {
                    // disable db somehow
                },
                postrun: function() {
                    // enable it again
                }
            }, {
                url: '/auth/token_refresh',
                // TODO fillin
            },{ 
                url:'/secret/admin'
            },{ 
                url'/secret'
            }
    ]
    },{
        status_code:'500',
        body: {'error': 'error creating user.'},
        routes: [
            {
                url: '/auth/register',
            }
        ]
    },{
        status_code:'500',
        body: {'error': 'error connecting to db.'},
        routes: ['/auth/register']
    },{
        status_code:'500',
        body:{'error':'error finding rights info.'},
        routes: ['/secret/admin']
    }
 //   */
  ];
};
