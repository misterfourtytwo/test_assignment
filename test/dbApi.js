const jwt = require("jsonwebtoken");
const secrets = require("./../secrets.js");

//const ObjectId = require('mongodb').ObjectId;

exports.db = {};

module.exports.create_user = (
  cb,
  { login = "test", password = "test", admin = "false" } = {}
) => {
  exports.db
    .collection("users")
    .insertOne({ login: login, password: password, admin: admin })
    .then(res => {
      console.log(`user ${login} created.`);
      cb();
    });
};

exports.remove_user = (cb, { login = "test" } = {}) => {
  exports.db
    .collection("users")
    .remove({ login: login })
    .then(res => {
      console.log(`user ${login} removed.`);
      cb();
    });
};

exports.get_id = (cb, { login = "test" } = {}) => {
  exports.db
    .collection("users")
    .findOne({ login: login })
    .then(res => {
      let id = res._id;
      console.log(`user ${login} id == ${id}.`);
      cb(null, id);
    });
};

exports.get_tokens = (cb, { login = "test", admin = "false" } = {}) => {
  var next = (err, id) => {
    const accessPayload = { id: id, admin: admin };

    const accessToken = jwt.sign(accessPayload, secrets.accessSecret, {
      expiresIn: 7200, // 2 hours
      issuer: "test_assignment"
    });

    const refreshPayload = { id: id };
    const refreshToken = jwt.sign(refreshPayload, secrets.refreshSecret, {
      issuer: "test_assignment"
    });

    const response = {
      "access-token": accessToken,
      "refresh-token": refreshToken
    };

    console.log("tokens created:" + response);
    cb(null, response);
  };
  this.get_id(next, { login });
};
